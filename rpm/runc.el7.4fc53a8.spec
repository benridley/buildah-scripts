%if 0%{?fedora} || 0%{?rhel} == 6
%global with_devel 1
%global with_bundled 0
%global with_debug 1
%global with_check 1
%global with_unit_test 1
%else
%global with_devel 0
%global with_bundled 1
%global with_debug 0
%global with_check 0
%global with_unit_test 0
%endif

%if 0%{?with_debug}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif

%global provider        github
%global provider_tld    com
%global project         opencontainers
%global repo            runc
# https://github.com/opencontainers/runc
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     %{provider_prefix}
%global git0 https://github.com/projectatomic/runc
%global commit0 4fc53a81fb7c994640722ac585fa9ca548971871
%global shortcommit0    %(c=%{commit0}; echo ${c:0:7})

Name: %{repo}
%if 0%{?fedora}
Epoch: 1
%endif
Version: 1.0.0
Release: 27.rc5.dev.git%{shortcommit0}.el7
Summary: CLI for running Open Containers
License: ASL 2.0
URL: https://%{provider_prefix}
Source0: %{git0}/archive/%{commit0}/%{name}-%{shortcommit0}.tar.gz
Requires: criu

# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires: %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang} >= 1.6.2
BuildRequires: git
BuildRequires: go-md2man
BuildRequires: libseccomp-devel

%if ! 0%{?with_bundled}
BuildRequires: golang(github.com/Sirupsen/logrus)
BuildRequires: golang(github.com/codegangsta/cli)
BuildRequires: golang(github.com/coreos/go-systemd/dbus)
BuildRequires: golang(github.com/coreos/go-systemd/util)
BuildRequires: golang(github.com/docker/docker/pkg/mount)
BuildRequires: golang(github.com/docker/docker/pkg/symlink)
BuildRequires: golang(github.com/docker/docker/pkg/term)
BuildRequires: golang(github.com/docker/docker/pkg/units)
BuildRequires: golang(github.com/godbus/dbus)
BuildRequires: golang(github.com/golang/protobuf/proto)
BuildRequires: golang(github.com/opencontainers/specs)
BuildRequires: golang(github.com/syndtr/gocapability/capability)
%endif

%description
The runc command can be used to start containers which are packaged
in accordance with the Open Container Initiative's specifications,
and to manage containers running under runc.

%if 0%{?with_devel}
%package devel
Summary:       %{summary}
BuildArch:     noarch

%if 0%{?with_check}
BuildRequires: golang(github.com/Sirupsen/logrus)
BuildRequires: golang(github.com/codegangsta/cli)
BuildRequires: golang(github.com/coreos/go-systemd/dbus)
BuildRequires: golang(github.com/coreos/go-systemd/util)
BuildRequires: golang(github.com/docker/docker/pkg/mount)
BuildRequires: golang(github.com/docker/docker/pkg/symlink)
BuildRequires: golang(github.com/docker/docker/pkg/term)
BuildRequires: golang(github.com/docker/docker/pkg/units)
BuildRequires: golang(github.com/godbus/dbus)
BuildRequires: golang(github.com/golang/protobuf/proto)
BuildRequires: golang(github.com/opencontainers/specs)
BuildRequires: golang(github.com/seccomp/libseccomp-golang)
BuildRequires: golang(github.com/syndtr/gocapability/capability)
BuildRequires: golang(github.com/vishvananda/netlink)
%endif

Requires:      golang(github.com/Sirupsen/logrus)
Requires:      golang(github.com/coreos/go-systemd/dbus)
Requires:      golang(github.com/coreos/go-systemd/util)
Requires:      golang(github.com/docker/docker/pkg/mount)
Requires:      golang(github.com/docker/docker/pkg/symlink)
Requires:      golang(github.com/docker/docker/pkg/units)
Requires:      golang(github.com/godbus/dbus)
Requires:      golang(github.com/golang/protobuf/proto)
Requires:      golang(github.com/seccomp/libseccomp-golang)
Requires:      golang(github.com/syndtr/gocapability/capability)
Requires:      golang(github.com/vishvananda/netlink)

Provides:      golang(%{import_path}/libcontainer) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/apparmor) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/cgroups) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/cgroups/fs) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/cgroups/systemd) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/configs) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/configs/validate) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/criurpc) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/devices) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/integration) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/label) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/nsenter) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/seccomp) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/selinux) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/stacktrace) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/system) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/user) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/utils) = %{version}-%{release}
Provides:      golang(%{import_path}/libcontainer/xattr) = %{version}-%{release}

%description devel
The runc command can be used to start containers which are packaged
in accordance with the Open Container Initiative's specifications,
and to manage containers running under runc.

This package contains library source intended for
building other packages which use import path with
%{import_path} prefix.
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%package unit-test
Summary:         Unit tests for %{name} package
# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires:  %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}

%if 0%{?with_check}
#Here comes all BuildRequires: PACKAGE the unit tests
#in %%check section need for running
%endif

# test subpackage tests code from devel subpackage
Requires:        %{name}-devel = %{version}-%{release}

%description unit-test
The runc command can be used to start containers which are packaged
in accordance with the Open Container Initiative's specifications,
and to manage containers running under runc.

This package contains unit tests for project
providing packages with %{import_path} prefix.
%endif

%prep
%autosetup -Sgit -n %{name}-%{commit0}

%build
mkdir -p GOPATH
pushd GOPATH
    mkdir -p src/%{provider}.%{provider_tld}/%{project}
    ln -s $(dirs +1 -l) src/%{import_path}
popd

pushd GOPATH/src/%{import_path}
export GOPATH=%{gopath}:$(pwd)/GOPATH
sed -i 's/seccomp/selinux seccomp/' Makefile
make all
popd

pushd man
./md2man-all.sh
popd

%install
install -d -p %{buildroot}%{_bindir}
install -p -m 755 %{name} %{buildroot}%{_bindir}

install -d -p %{buildroot}%{_mandir}/man8
install -p -m 644 man/man8/* %{buildroot}%{_mandir}/man8

# source codes for building projects
%if 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
# find all *.go but no *_test.go files and generate devel.file-list
for file in $(find . -iname "*.go" \! -iname "*_test.go" | grep -v "^./Godeps") ; do
    echo "%%dir %%{gopath}/src/%%{import_path}/$(dirname $file)" >> devel.file-list
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$(dirname $file)
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> devel.file-list
done
for file in $(find . -iname "*.proto" | grep -v "^./Godeps") ; do
    echo "%%dir %%{gopath}/src/%%{import_path}/$(dirname $file)" >> devel.file-list
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$(dirname $file)
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> devel.file-list
done
%endif

# testing files for this project
%if 0%{?with_unit_test} && 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
# find all *_test.go files and generate unit-test.file-list
for file in $(find . -iname "*_test.go" | grep -v "^./Godeps"); do
    echo "%%dir %%{gopath}/src/%%{import_path}/$(dirname $file)" >> devel.file-list
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$(dirname $file)
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> unit-test.file-list
done
%endif

%if 0%{?with_devel}
sort -u -o devel.file-list devel.file-list
%endif

%check
%if 0%{?with_check} && 0%{?with_unit_test} && 0%{?with_devel}
%if ! 0%{?with_bundled}
export GOPATH=%{buildroot}/%{gopath}:%{gopath}
%else
export GOPATH=%{buildroot}/%{gopath}:$(pwd)/Godeps/_workspace:%{gopath}
%endif

%if ! 0%{?gotest:1}
%global gotest go test
%endif

# FAIL: TestFactoryNewTmpfs (0.00s), factory_linux_test.go:59: operation not permitted
#%%gotest %%{import_path}/libcontainer
%gotest %{import_path}/libcontainer/cgroups
%gotest %{import_path}/libcontainer/cgroups/fs
%gotest %{import_path}/libcontainer/configs
%gotest %{import_path}/libcontainer/devices
# undefined reference to `nsexec'
#%%gotest %%{import_path}/libcontainer/integration
%gotest %{import_path}/libcontainer/label
# Unable to create tstEth link: operation not permitted
#%%gotest %%{import_path}/libcontainer/netlink
# undefined reference to `nsexec'
#%%gotest %%{import_path}/libcontainer/nsenter
%gotest %{import_path}/libcontainer/selinux
%gotest %{import_path}/libcontainer/stacktrace
%gotest %{import_path}/libcontainer/user
%gotest %{import_path}/libcontainer/utils
%gotest %{import_path}/libcontainer/xattr
%endif

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc MAINTAINERS_GUIDE.md PRINCIPLES.md README.md CONTRIBUTING.md
%{_bindir}/%{name}
%{_mandir}/man8/%{name}*

%if 0%{?with_devel}
%files devel -f devel.file-list
%license LICENSE
%doc MAINTAINERS_GUIDE.md PRINCIPLES.md README.md CONTRIBUTING.md
%dir %{gopath}/src/%{provider}.%{provider_tld}/%{project}
%dir %{gopath}/src/%{import_path}
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%files unit-test -f unit-test.file-list
%license LICENSE
%doc MAINTAINERS_GUIDE.md PRINCIPLES.md README.md CONTRIBUTING.md
%endif

%changelog
* Tue Apr 03 2018 PixelDrift.NET Support <support@pixeldrift.net> - 1.0.0-27.rc5.git4fc53a8
- Bump to rc5 release from upstream

* Wed Jan 24 2018 Dan Walsh <dwalsh@redhat.name> - 1.0.0-26.rc4.git9f9c962
- Bump to the latest from upstream

* Mon Dec 18 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-25.rc4.gite6516b3
- built commit e6516b3

* Fri Dec 15 2017 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.0.0-24.rc4.dev.gitc6e4a1e.1
- rebase to c6e4a1ebeb1a72b529c6f1b6ee2b1ae5b868b14f
- https://github.com/opencontainers/runc/pull/1651

* Tue Dec 12 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-23.rc4.git1d3ab6d
- Resolves: #1524654

* Sun Dec 10 2017 Dan Walsh <dwalsh@redhat.name> - 1.0.0-22.rc4.git1d3ab6d
- Many Stability fixes
- Many fixes for rootless containers
- Many fixes for static builds

* Thu Nov 09 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-21.rc4.dev.gitaea4f21
- enable debuginfo and include -buildmode=pie for go build

* Tue Nov 07 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-20.rc4.dev.gitaea4f21
- use Makefile

* Tue Nov 07 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-19.rc4.dev.gitaea4f21
- disable debuginfo temporarily

* Fri Nov 03 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-18.rc4.dev.gitaea4f21
- enable debuginfo

* Wed Oct 25 2017 Dan Walsh <dwalsh@redhat.name> - 1.0.0-17.rc4.gitaea4f21
- Add container-selinux prerequires to make sure runc is labeled correctly

* Thu Oct 19 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-16.rc4.dev.gitaea4f21
- correct the release tag "rc4dev" -> "rc4.dev" cause I'm OCD

* Mon Oct 16 2017 Dan Walsh <dwalsh@redhat.com> - 1.0.0-15.rc4dev.gitaea4f21
- Use the same checkout as Fedora for lates CRI-O

* Fri Sep 22 2017 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.0.0-14.rc4dev.git84a082b
- rebase to 84a082bfef6f932de921437815355186db37aeb1

* Tue Jun 13 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-13.rc3.gitd40db12
- Resolves: #1479489
- built commit d40db12

* Tue Jun 13 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-12.1.gitf8ce01d
- disable s390x temporarily because of indefinite wait times on brew

* Tue Jun 13 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-11.1.gitf8ce01d
- correct previous bogus date :\

* Mon Jun 12 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-10.1.gitf8ce01d
- Resolves: #1441737 - run sysctl_apply for sysctl knob

* Tue May 09 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-9.1.gitf8ce01d
- Resolves: #1447078 - change default root path
- add commit e800860 from runc @projectatomic/change-root-path

* Fri May 05 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-8.1.gitf8ce01d
- Resolves: #1441737 - enable kernel sysctl knob /proc/sys/fs/may_detach_mounts

* Thu Apr 13 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-7.1.gitf8ce01d
- Resolves: #1429675
- built @opencontainers/master commit f8ce01d

* Wed Apr 05 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-6.gite800860
- Resolves: #1438911 - change default --root to /run/runc-ctrs

* Tue Mar 21 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-5.git75f8da7
- built @mrunalp/1_rc3 commit 75f8da7

* Thu Mar 16 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-4.gitee992e5
- built @projectatomic/master commit ee992e5

* Fri Feb 24 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-3.rc2
- Resolves: #1426674
- built projectatomic/runc_rhel_7 commit 5d93f81

* Mon Feb 06 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-2.rc2
- Resolves: #1419702 - rebase to latest upstream master
- built commit b263a43

* Wed Jan 11 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-1.rc2
- Resolves: #1412239 - *CVE-2016-9962* - set init processes as non-dumpable,
runc patch from Michael Crosby <crosbymichael@gmail.com>

* Wed Sep 07 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.1-6
- Resolves: #1373980 - rebuild for 7.3.0

* Sat Jun 25 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.1-5
- build with golang >= 1.6.2

* Tue May 31 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.1-4
- release tags were inconsistent in the previous build

* Tue May 31 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.1-1
- Resolves: #1341267 - rebase runc to v0.1.1

* Tue May 03 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.0-3
- add selinux build tag
- add BR: libseccomp-devel

* Tue May 03 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.0-2
- Resolves: #1328970 - add seccomp buildtag

* Tue Apr 19 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.0-1
- Resolves: rhbz#1328616 - rebase to v0.1.0

* Tue Mar 08 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.0.8-1.git4155b68
- Resolves: rhbz#1277245 - bump to 0.0.8
- Resolves: rhbz#1302363 - criu is a runtime dep
- Resolves: rhbz#1302348 - libseccomp-golang is bundled in Godeps
- manpages included

* Wed Nov 25 2015 jchaloup <jchaloup@redhat.com> - 1:0.0.5-0.1.git97bc9a7
- Update to 0.0.5, introduce Epoch for Fedora due to 0.2 version instead of 0.0.2

* Fri Aug 21 2015 Jan Chaloupka <jchaloup@redhat.com> - 0.2-0.2.git90e6d37
- First package for Fedora
  resolves: #1255179