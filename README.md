# buildah minimal container image build scripts
Scripts to build container images using buildah (https://github.com/projectatomic/buildah)

| Script   | Description | Platforms tested | buildah reported container size | Compressed OCI archive size (xz -9)|
| -------- | -------- | -------- | -------- | -------- |
| [el7.minimal.sh](https://gitlab.com/pixdrift/buildah-scripts/blob/master/el7.minimal.sh) | Minimal install CentOS 7 base image | RHEL 7.4, OL 7.4, CentOS 7.4 | 59.5MB | 23MB |
| [el8.minimal.sh](https://gitlab.com/pixdrift/buildah-scripts/blob/master/el8.minimal.sh) | Minimal install CentOS 8 base image | CentOS 8-Stream | 56.8MB | 18MB |
| [el7.buildah.rpmbuild.sh](https://gitlab.com/pixdrift/buildah-scripts/blob/master/el7.buildah.rpmbuild.sh) | rpmbuild in container example | CentOS 7.4 | 253MB | |
| [el7.ansible.minimal.sh](https://gitlab.com/pixdrift/buildah-scripts/blob/master/el7.ansible.minimal.sh) | Using pip on host to install python packages into container | OL 7.4, CentOS 7.4 | 156MB | |
<br />

# buildah minimal container image vs. UBI minimal container image size comparison
Note: This is a point in time comparison and may change over time.
  
| Image | buildah reported container size | Compressed OCI archive size (xz -9)| RPM Package count |
| -------- | -------- | -------- | -------- |
| ubi7-minimal| 83MB | 32MB | 93 |
| **buildah el7-minimal** | **59.5 MB** | **23MB** | 42 |
| ubi8-minimal| 107MB | 34MB | 103 |
| **buildah el8-minimal** | **56.8 MB** | **18MB** | 37 |
<br />

# No package management tools included in minimal container images
Although UBI's include microdnf for package management, package management tools are intentionally excluded from these minimal container images. All package installation/configuration can occur at container build time utilising package management tooling external to the container (providing the build environment can support the packaging method). The trade off is that the containers become less portable, but the size is also reduced.

If inspection of installed packages of the containers is required (eg. for version comparison), `buildah` can mount the container image and inspect the RPM database using standard RPM tools on the build server.  
### Inspecting container installed RPMs from outside the container:
```
# Create a new temporary working container from the existing el7-minimal image
rpminspect=$(buildah from el7-minimal)

# Mount the image filesystem
rpmmount=$(buildah mount ${rpminspect})

# List installed RPM packages from container RPM database using `rpm` binary on build server
rpm -qa --dbpath=${rpmmount}/var/lib/rpm

# Destroy temporary working container
buildah rm ${rpminspect}
```

#### The Catch:
Unfortunately, due to changes to RPM between el7 and el8, the database format used by RPM in el8 is not compatible when attempting the above method using an el7 version of RPM to read the database off a container that was built using an el8 version of dnf/RPM.  
  
An error similar to the following will be returned:
```
rpm -qa --dbpath=${rpmmount}/var/lib/rpm
error: db5 error(5) from dbenv->open: Input/output error
error: cannot open Packages index using db5 - Input/output error (5)
error: cannot open Packages database in /var/lib/containers/storage/overlay/21ffd37227f616a4edda07cae3ae3f495afb9548329af03418da640f351d155d/merged/var/lib/rpm
error: db5 error(5) from dbenv->open: Input/output error
error: cannot open Packages database in /var/lib/containers/storage/overlay/21ffd37227f616a4edda07cae3ae3f495afb9548329af03418da640f351d155d/merged/var/lib/rpm
```
The workaround for this issue is to use the version of RPM equal to the highest container RPM version requirement on the build server for all container builds.  
<br />
# Configuring buildah container image builds to use a specific upstream yum repository
One of the benefits of using `yum` from outside the container to pull packages is the ability to target a library of upstream software repositories. Using this mechanism, building containers for specific minor *el* versions (using EUS) or even different major OS versions is possible. The following example describes this process.

### Building an el8 container image on an el7 build server
In this example, an *el7* (7.6) build server is used to build an *el8* container image. This is the method used in the `el8.minimal.sh` file in this repository.  
  
Before building the new container, configure a `yum` source on the build server in the standard yum repository location `/etc/yum.repos.d`  
  
**Note:** This new repository is configured with `enabled=0` to ensure that the repository definition doesn't interfere with the `yum` operation on the *el7* build server. An alternate approach is to store yum configuration files for container builds in a separate location away from the build server's configuration and then referencing them using `yum --config=`:
  
**Note:** The GPG key for the `el8` repository will be downloaded by `yum`. The `centos-release` rpm is not required to configure these keys and would conflict with the `centos-release` package of the *el7* build server.
```
[8-stream]
enabled=0
name=CentOS8-stream
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os&infra=$infra
baseurl=http://mirror.centos.org/centos/8-stream/BaseOS/$basearch/os/
gpgcheck=1
gpgkey=https://www.centos.org/keys/RPM-GPG-KEY-CentOS-Official
```

Once the new repository is configured, the definition can be referenced by name `8-stream` from `yum`. For the `el8.minimal.sh` container image build script, a minor change is implemented to reference this specific repository by name in the yum step.

`yum` install command changed from:  
```
yum install --installroot ${scratchmnt} bash coreutils --releasever 8...
```

to:  
```
yum --disablerepo="*" --enablerepo="8-stream" install --installroot ${scratchmnt} bash coreutils --releasever 8...
```

This instructs yum to disable all repositories globally, and then explicitly enable the `8-stream` repository for package installation.  
  
**Note:** In this example the releasever is also changed in the `el8.minimal.sh` script, but this can be hardcoded in the repository file.
<br />
  
On `yum` execution, the selection of the spefic repository can be seen in command output:
```
++ buildah from scratch
+ newcontainer=working-container-4
++ buildah mount working-container-4
+ scratchmnt=/var/lib/containers/storage/overlay/259bc3ae15f6acf5a9ee815e6647156cbc22db2f8a121e3dab26b7eccbd90081/merged
+ yum '--disablerepo=*' --enablerepo=8-stream install --installroot /var/lib/containers/storage/overlay/259bc3ae15f6acf5a9ee815e6647156cbc22db2f8a121e3dab26b7eccbd90081/merged bash coreutils --releasever 8 --setopt=tsflags=nodocs --setopt=override_install_langs=en_US.utf8 -y
Loaded plugins: fastestmirror
Determining fastest mirrors
8-stream                                                                                                                                                                                                             | 3.9 kB  00:00:00
(1/2): 8-stream/8/x86_64/group_gz                                                                                                                                                                                    |  55 kB  00:00:00
(2/2): 8-stream/8/x86_64/primary_db                                                                                                                                                                                  | 1.7 MB  00:00:06
Resolving Dependencies
...
```
<br />

# Troubleshooting buildah
#### Issue: buildah: symbol lookup error: buildah: undefined symbol: seccomp_api_set
**Summary:**  
Some versions of the CentOS 7 RPM shipped with a missing dependency in the buildah RPM, resulting in an older version of libseccomp being selected to meet dependency requirement. As a result, execution of the `buildah` binary fails with the following message.
```
# buildah version
buildah: symbol lookup error: buildah: undefined symbol: seccomp_api_set
```

**Resolution:**  
The required RPM for `libseccomp` can be downloaded from [CentOS Community Build Server container repository](https://cbs.centos.org/repos/virt7-container-common-candidate/x86_64/os/Packages/)
```
# yum install libseccomp
```
<br />  
  
# Related links

#### CentOS Community Build Server container repository
[https://cbs.centos.org/repos/virt7-container-common-candidate/x86_64/os/Packages/](https://cbs.centos.org/repos/virt7-container-common-candidate/x86_64/os/Packages/)