#!/bin/bash

#
# Generate minimal container image (~234MB) that includes buildah, built with buildah (https://github.com/projectatomic/buildah)
#

set -ex

# start new container from scratch
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# install the packages
yum install --installroot ${scratchmnt} bash buildah coreutils --releasever 7 --setopt=tsflags=nodocs --setopt=override_install_langs=en_US.utf8 -y

# Clean up yum cache
if [ -d "${scratchmnt}" ]; then
  rm -rf "${scratchmnt}"/var/cache/yum
fi

# configure container label and entrypoint
buildah config --label name=el7-buildah ${newcontainer}
buildah config --cmd /bin/bash ${newcontainer}

# commit the image
buildah unmount ${newcontainer}
buildah commit ${newcontainer} el7-buildah
