#!/bin/bash

#
# buildah rpm builder - demonstration of building an rpm .spec from a git repo using a buildah container rather than mock
# TODO: increment version of rpm release, update changelog with commit info
# Example execution: podman run -v /rpm:/rpm el7-rpmbuild 4743c2e7f132010a01de2d9cf96d10244fced423

set -ex

# start new container from scratch
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# install the packages
yum install --installroot ${scratchmnt} bash coreutils git rpm-build yum yum-plugin-ovl yum-utils --releasever 7 --setopt=tsflags=nodocs --setopt=override_install_langs=en_US.utf8 -y

# clean up yum cache
if [ -d "${scratchmnt}" ]; then
  rm -rf "${scratchmnt}"/var/cache/yum
fi

# EOF quoted to avoid stripping of variables
cat << "EOF" > /${scratchmnt}/root/entrypoint.sh
#!/bin/bash
set -ex

git_commit=${1}
short_commit=${git_commit:0:7}

mkdir -p /root/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS} /rpm

git clone https://github.com/projectatomic/buildah.git
cd buildah
git checkout ${git_commit}

# capture date of commit for use in RPM release
commit_date=$(date +%Y%m%d%H%M -d @$(git show -s --format=%ct ${git_commit}))
cp contrib/rpm/buildah.spec /root/rpmbuild/SPECS/

mkdir /tmp/buildah-${git_commit}

git archive ${git_commit} | tar -x -C /tmp/buildah-${git_commit}/
tar czpvf /root/rpmbuild/SOURCES/buildah-${short_commit}.tar.gz -C /tmp buildah-${git_commit}

# update the git commit in the spec file
sed -i "s/REPLACEWITHCOMMITID/${git_commit}/" /root/rpmbuild/SPECS/buildah.spec

# change release to date of commit so RPM install/upgrade will work correctly with package versions in yum/RPM
sed -i "s/1.git/${commit_date}.git/" /root/rpmbuild/SPECS/buildah.spec

yum-builddep -y /root/rpmbuild/SPECS/buildah.spec 
rpmbuild -ba /root/rpmbuild/SPECS/buildah.spec

# move finished product to /rpm directory
mv /root/rpmbuild/RPMS/x86_64/*.rpm /rpm
mv /root/rpmbuild/SRPMS/*.rpm /rpm
EOF

# fix permission to allow execute so we can use it as the container entrypoint
chmod 700 /${scratchmnt}/root/entrypoint.sh

# remove centos from rpm dist macro so final package doesn't have centos in name
sed -i "s/.el7.centos/.el7/" /${scratchmnt}/etc/rpm/macros.dist

# configure container label and entrypoint
buildah config --label name=el7-rpmbuild ${newcontainer}
buildah config --entrypoint /root/entrypoint.sh ${newcontainer}

# commit the image
buildah unmount ${newcontainer}
buildah commit ${newcontainer} el7-rpmbuild

# clean up the working container
buildah rm ${newcontainer}