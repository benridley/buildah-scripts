#!/bin/bash

#
# Generate minimal container image (~57MB) from RHEL7/OL7/CentOS7 repo using buildah (https://github.com/projectatomic/buildah)
#

set -ex

# start new container from scratch
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# install the packages
yum install --installroot ${scratchmnt} bash coreutils --releasever 7 --setopt=tsflags=nodocs --setopt=override_install_langs=en_US.utf8 -y

# Clean up yum cache
if [ -d "${scratchmnt}" ]; then
  rm -rf "${scratchmnt}"/var/cache/yum
fi

# configure container label and entrypoint
buildah config --label name=el7-minimal ${newcontainer}
buildah config --cmd /bin/bash ${newcontainer}

# commit the image
buildah unmount ${newcontainer}
buildah commit ${newcontainer} el7-minimal

# clean up the working container
buildah rm ${newcontainer}